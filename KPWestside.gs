// The report destination folder
var KPWESTSIDE_FOLDER_ID = '0BzEumup9NOKVWk9ESHlJeGZvUlE';

// The report template file
var KPWESTSIDE_TEMPLATE_ID = '1RDlhWSw_sVtB97If1IA2wF_37XvN0i-h3YfJn8ljmVA';

function generateKPWestside() {
  var template = DriveApp.getFileById(KPWESTSIDE_TEMPLATE_ID)
  var newName = generateName();
  var newFile = copyTemplate(template, newName);
  copyDataToSheet(newFile);
}

// Copy template to destination folder.  If file already exists, return that file
function copyTemplate(template, newName) {
  var folder = DriveApp.getFolderById(KPWESTSIDE_FOLDER_ID);
  var files = folder.getFilesByName(newName);
  var file = null;
  if (files.hasNext()) {
    file = files.next();
  }
  else {
    file = template.makeCopy(newName, folder)
  }
  return file;
}

// Copy analytics data to the new file
function copyDataToSheet(newFile) {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName('System Utilization');
  var sourceDataRange = sheet.getDataRange();
  var sourceSheetValues = sourceDataRange.getValues();
  var sourceRows = sourceDataRange.getNumRows();
  var sourceColumns = sourceDataRange.getNumColumns();
  
  var destination = SpreadsheetApp.open(newFile);
  var destinationSheet = destination.getSheetByName('System Utilization');
  if(!destinationSheet) {
    destinationSheet = destination.insertSheet('System Utilization', 0);
  }
  destinationSheet.getDataRange().offset(0, 0, sourceRows, sourceColumns).setValues(sourceSheetValues);
}

// Generate name for last month
function generateName() {
  var d = new Date();
  d.setDate(1);
  d.setMonth(d.getMonth()-1);
  var TimeZone = Session.getScriptTimeZone();
  var DTStamp = Utilities.formatDate(d, TimeZone, 'yyyyMM');
  return "KPWestside-" + DTStamp;
}